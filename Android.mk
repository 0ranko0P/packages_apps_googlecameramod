#
# version: MGC_MAX_2.1
#

ifeq ($(TARGET_DEVICE),sagit)

LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)

LOCAL_MODULE := GoogleCameraMod-sagit
LOCAL_MODULE_TAGS := optional

LOCAL_SRC_FILES := GoogleCamera.apk
LOCAL_CERTIFICATE := platform
LOCAL_DEX_PREOPT := false
LOCAL_MODULE_CLASS := APPS
LOCAL_MODULE_SUFFIX := $(COMMON_ANDROID_PACKAGE_SUFFIX)
LOCAL_OVERRIDES_PACKAGES := Snap Camera2 SnapdragonCamera

LOCAL_PRODUCT_MODULE := true

include $(BUILD_PREBUILT)

endif
